// src/app/services/auth.service.ts
import {Injectable} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {Observable} from 'rxjs/Observable';
import {Platform, LoadingController} from 'ionic-angular';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';
import {FirebaseApi} from '../firebaseApi/firebaseApi.service';

@Injectable()
export class AuthService {
  private user: Observable<firebase.User>;

  constructor(private _firebaseAuth: AngularFireAuth,
              private platform: Platform,
              private facebook: Facebook,
              private loadingCtrl: LoadingController,
              private firebaseApi: FirebaseApi,) {
    this.user = _firebaseAuth.authState;
  }

  createUser(user: {email: string, password: string}): Promise<any> {
    return new Promise((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(user.email, user.password).then((data) => {
        this.firebaseApi.post('users', {}).subscribe((data) => {
          resolve();
        });
      }, (error) => {
        reject(error);
      });
    });
  }

  signInWithEmailAndPassword(user: {email: string, password: string}): Promise<any> {
    return new Promise((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(user.email, user.password).then((data) => {
        console.log('login user data', data);
        resolve(data);
      }, (error) => {
        reject(error);
      });
    });
  }

  signInWithFacebook(): Promise<any> {
    console.log('this.platform', this.platform);
    if (this.platform.is('cordova')) {
      return this.signInWithFacebookCordova();
    } else {
      return <Promise<any>> this.signInWithFacebookBrowser();
    }
  }

  signInWithFacebookBrowser() {
    return new Promise((resolve, reject) => {
      this._firebaseAuth.auth.signInWithPopup(
        new firebase.auth.FacebookAuthProvider()
      ).then((firebaseData) => {
        resolve(firebaseData.credential.accessToken);
      }, (error) => {
        reject(error);
      });
    });
  }

  signInWithGoogle() {
    if (this.platform.is('cordova')) {
      // TODO: mobile google auth
      //return this.signInWithFacebookCordova();
    } else {
      return <Promise<any>> this.signInWithGoogleBrowser();
    }
  }

  signInWithGoogleBrowser() {
    return new Promise((resolve, reject) => {
      this._firebaseAuth.auth.signInWithPopup(
        new firebase.auth.GoogleAuthProvider()
      ).then((firebaseData) => {
        resolve(firebaseData.credential.accessToken);
      }, (error) => {
        reject(error);
      });
    });
  }

  signInWithFacebookCordova() {
    return new Promise((resolve, reject) => {
      this.facebook.login(['email', 'public_profile', 'user_friends'])
        .then((facebookData: FacebookLoginResponse) => {
          const credential = firebase.auth.FacebookAuthProvider
            .credential(facebookData.authResponse.accessToken);
          firebase.auth().signInWithCredential(credential).then((firebaseData) => {
            resolve(facebookData.authResponse.accessToken);
          });
        }, (error) => {
          resolve(error);
        });
    });
  }

  connectFacebookToFirebase(token) {
    return new Promise((resolve, reject) => {
      const loading = this.loadingCtrl.create({
        content: 'Loading...'
      });
      loading.present();
      setTimeout(() => {
        this.firebaseApi.post('users', {
          facebook: token,
        }).subscribe((response) => {
          loading.dismiss();
          console.log('response after user post', response);
          resolve(response);
        }, (error) => {
          console.log('error after user post', error);
          loading.dismiss();
          reject(error);
        });
      }, 800);
    });
  }
}
