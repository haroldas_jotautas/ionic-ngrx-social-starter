import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {IonicPage, NavController, ToastController} from 'ionic-angular';

import {AuthService} from "../../providers/auth/auth.service";
import {Store} from "@ngrx/store";
import {IAppState} from "../../redux/app.reducers";
import * as AuthActions from '../../redux/actions/auth.actions';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { name: string, email: string, password: string } = {
    name: 'Test Human',
    email: 'test@example.com',
    password: 'test12'
  };

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              public auth: AuthService,
              private store: Store<IAppState>) {

    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  doSignup() {
    this.store.dispatch(new AuthActions.SignUpEmail({email: this.account.email, password: this.account.password}));
  }
}
