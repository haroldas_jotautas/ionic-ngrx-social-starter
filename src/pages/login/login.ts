import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {IonicPage, NavController, ToastController} from 'ionic-angular';
import {Store} from '@ngrx/store';
import {Observable} from "rxjs/Observable";

import {IAppState} from '../../redux/app.reducers';
import {AuthService} from '../../providers/auth/auth.service';
import * as fromAuth from '../../redux/reducers/auth.reducers';
import * as AuthActions from '../../redux/actions/auth.actions';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage implements OnInit {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type

  account: { email: string, password: string } = {
    email: 'test@example.com',
    password: 'test'
  };
  // Our translated text strings
  private loginErrorString: string;

  authState: Observable<fromAuth.State>;

  constructor(public navCtrl: NavController,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              public auth: AuthService,
              private store: Store<IAppState>) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  ngOnInit(): void {
  }

  doLogin() {
    this.store.dispatch(new AuthActions.SignInEmail(this.account));
  }

  signInWithFacebook() {
    this.store.dispatch(new AuthActions.SignInFB({}));
  }

  signInWithGoogle() {
    this.store.dispatch(new AuthActions.SignInGoogle({}));
  }
}
