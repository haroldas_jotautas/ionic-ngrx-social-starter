import {Injectable} from '@angular/core';
import {Effect, Actions, toPayload} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import {of} from 'rxjs/observable/of';
import {fromPromise} from 'rxjs/observable/fromPromise';
import * as firebase from 'firebase';


import * as AuthActions from '../actions/auth.actions';
import {AuthService} from '../../providers/auth/auth.service';
import {App} from "ionic-angular";
import {MainPage} from "../../pages/pages";


@Injectable()
export class AuthEffects {
  constructor(private actions$: Actions,
              private authService: AuthService,
              public app: App,) {
  }

  @Effect() public fbSignup$ = this.actions$.ofType(AuthActions.ActionTypes.SIGN_IN_FB)
    .switchMap(() => {
      return fromPromise(this.authService.signInWithFacebook())
        .switchMap(() => {
          return fromPromise(firebase.auth().currentUser.getIdToken())
            .mergeMap((accessToken) => {
              this.app.getActiveNavs()[0].push(MainPage);
              return [
                {
                  type: AuthActions.ActionTypes.CREATE_DB_USER,
                  payload: accessToken
                },
                {
                  type: AuthActions.ActionTypes.SIGN_IN_FB_SUCCESS,
                  payload: accessToken
                },
              ]
            });
        })
        .catch((e) => of(new AuthActions.SignInFbFail(e)));
    });


  @Effect({dispatch: false})
  public createFirebaseUser$ = this.actions$.ofType(AuthActions.ActionTypes.CREATE_DB_USER)
    .switchMap((accessToken) => {
      return fromPromise(this.authService.connectFacebookToFirebase(accessToken))
        .map(() => {
          this.app.getActiveNavs()[0].push(MainPage);
        });
    });

  @Effect() public emailSignUp$ = this.actions$.ofType(AuthActions.ActionTypes.SIGN_UP_EMAIL)
    .switchMap((action: AuthActions.SignUpEmail) => {
      console.log('TRY_SIGN_up_email effect');
      return fromPromise(this.authService.createUser(action.payload))
        .switchMap(() => {
          return fromPromise(firebase.auth().currentUser.getIdToken())
            .mergeMap((accessToken) => {
              this.app.getActiveNavs()[0].push(MainPage);
              return [
                {
                  type: AuthActions.ActionTypes.SIGN_UP_EMAIL_SUCCESS,
                  payload: accessToken
                },
              ]
            })
        })
        .catch((e) => of(new AuthActions.SignUpEmailFail(e)));
    });

  @Effect() public emailSignIn$ = this.actions$.ofType(AuthActions.ActionTypes.SIGN_IN_EMAIL)
    .switchMap((action: AuthActions.SignInEmail) => {
      console.log('TRY_SIGN_in_email effect');
      return fromPromise(this.authService.signInWithEmailAndPassword(action.payload))
        .switchMap(() => {
          return fromPromise(firebase.auth().currentUser.getIdToken())
            .mergeMap((accessToken) => {
              this.app.getActiveNavs()[0].push(MainPage);
              return [
                {
                  type: AuthActions.ActionTypes.SIGN_IN_EMAIL_SUCCESS,
                  payload: accessToken
                },
              ]
            })
        })
        .catch((e) => of(new AuthActions.SignInEmailFail(e)));
    });

  @Effect({dispatch: false})
  public authFailMessage$ = this.actions$.ofType(AuthActions.ActionTypes.SIGN_IN_FB_FAIL, AuthActions.ActionTypes.SIGN_UP_EMAIL_FAIL, AuthActions.ActionTypes.SIGN_IN_EMAIL_FAIL,)
    .map((action: AuthActions.SignInFbFail) => {
      // TODO: implement error handling
      console.log('authFailMessage', action.payload);
    });

  @Effect() public googleSignup$ = this.actions$.ofType(AuthActions.ActionTypes.SIGN_IN_GOOGLE)
    .switchMap(() => {
      return fromPromise(this.authService.signInWithGoogle())
        .switchMap(() => {
          return fromPromise(firebase.auth().currentUser.getIdToken())
            .mergeMap((accessToken) => {
              this.app.getActiveNavs()[0].push(MainPage);
              return [
                {
                  type: AuthActions.ActionTypes.CREATE_DB_USER,
                  payload: accessToken
                },
                {
                  type: AuthActions.ActionTypes.SIGN_IN_FB_SUCCESS,
                  payload: accessToken
                },
              ]
            });
        })
        .catch((e) => of(new AuthActions.SignInFbFail(e)));
    });


}
