import { Action } from '@ngrx/store';
import { type } from '../util';

export const ActionTypes = {
    CREATE_DB_USER: type('[Auth] Create User'),
    SIGN_IN_FB: type('[Auth] Sign In Fb'),
    SIGN_IN_FB_SUCCESS: type('[Auth] Sign In Fb Success'),
    SIGN_IN_FB_FAIL: type('[Auth] Sign In Fb Fail'),
    SIGN_IN_GOOGLE: type('[Auth] Sign In Google'),
    SIGN_IN_GOOGLE_SUCCESS: type('[Auth] Sign In Google Success'),
    SIGN_IN_GOOGLE_FAIL: type('[Auth] Sign In Google Fail'),
    SIGN_UP_EMAIL: type('[Auth] Sign Up Email'),
    SIGN_UP_EMAIL_SUCCESS: type('[Auth] Sign Up Email Success'),
    SIGN_UP_EMAIL_FAIL: type('[Auth] Sign Up Email Fail'),
    SIGN_IN_EMAIL: type('[Auth] Sign In Email'),
    SIGN_IN_EMAIL_SUCCESS: type('[Auth] Sign In Email Success'),
    SIGN_IN_EMAIL_FAIL: type('[Auth] Sign In Email Fail'),
    SET_TOKEN: type('[Auth] Set Token'),
    LOGOUT: type('[Auth] Login FAIL'),
};

export class CreateUser implements Action {
  readonly type = ActionTypes.CREATE_DB_USER;

  constructor(public payload: any) {}
}

export class SignInFB implements Action {
  readonly type = ActionTypes.SIGN_IN_FB;

  constructor(public payload: any) {}
}

export class SignInFbSuccess implements Action {
    type = ActionTypes.SIGN_IN_FB_SUCCESS;
    constructor(public payload: any = {}) { }
}

export class SignInFbFail implements Action {
  type = ActionTypes.SIGN_IN_FB_FAIL;
  constructor(public payload: any = {}) { }
}

export class SignInGoogle implements Action {
  readonly type = ActionTypes.SIGN_IN_GOOGLE;

  constructor(public payload: any) {}
}

export class SignInGoogleSuccess implements Action {
  type = ActionTypes.SIGN_IN_GOOGLE_SUCCESS;
  constructor(public payload: any = {}) { }
}

export class SignInGoogleFail implements Action {
  type = ActionTypes.SIGN_IN_GOOGLE_FAIL;
  constructor(public payload: any = {}) { }
}

export class SignUpEmail implements Action {
  readonly type = ActionTypes.SIGN_UP_EMAIL;

  constructor(public payload: any) {}
}

export class SignUpEmailSuccess implements Action {
  readonly type = ActionTypes.SIGN_UP_EMAIL_SUCCESS;

  constructor(public payload: any) {}
}

export class SignUpEmailFail implements Action {
  readonly type = ActionTypes.SIGN_UP_EMAIL_FAIL;

  constructor(public payload: any) {}
}

export class SignInEmail implements Action {
  readonly type = ActionTypes.SIGN_IN_EMAIL;

  constructor(public payload: any) {}
}

export class SignInEmailSuccess implements Action {
  readonly type = ActionTypes.SIGN_UP_EMAIL_SUCCESS;

  constructor(public payload: any) {}
}

export class SignInEmailFail implements Action {
  readonly type = ActionTypes.SIGN_UP_EMAIL_FAIL;

  constructor(public payload: any) {}
}

export class SetToken implements Action {
  readonly type = ActionTypes.SET_TOKEN;

  constructor(public payload: any) {}
}

export class LogoutAction implements Action {
    type = ActionTypes.LOGOUT;

    constructor(public payload: any) { }
}


export type AuthActions =
  SignInFB |
  SignInFbSuccess |
  SignInFbFail |
  SignInGoogle |
  SignInGoogleSuccess |
  SignInGoogleFail |
  SignUpEmail |
  SignUpEmailSuccess |
  SignUpEmailFail |
  SignInEmail |
  SignInEmailSuccess |
  SignInEmailFail |
  LogoutAction |
  CreateUser |
  SetToken ;
