import {ActionReducerMap} from "@ngrx/store";

import * as fromAuth from '../redux/reducers/auth.reducers';

export interface IAppState {
  auth: fromAuth.State,
}

export const reducers : ActionReducerMap<IAppState> = {
  auth: fromAuth.authReducer,
};
