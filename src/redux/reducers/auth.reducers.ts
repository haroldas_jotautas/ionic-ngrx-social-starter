import * as AuthActions from '../actions/auth.actions'

export interface State {
  token: string;
  authenticated: boolean;
}

const initialState: State = {
  token: null,
  authenticated: false,
};


export function authReducer(state = initialState, action: AuthActions.AuthActions) {
  switch (action.type) {
    case (AuthActions.ActionTypes.SIGN_UP_EMAIL_SUCCESS):
    case (AuthActions.ActionTypes.SIGN_IN_EMAIL_SUCCESS):
    case (AuthActions.ActionTypes.SIGN_IN_FB_SUCCESS):
      return {
        ...state,
        authenticated: true,
        token: action.payload
      };
    case (AuthActions.ActionTypes.LOGOUT):
      return {
        ...state,
        token: null,
        authenticated: false
      };
    case (AuthActions.ActionTypes.SET_TOKEN):
      return {
        ...state,
        token: action.payload
      };
    default:
      return state;
  }
}
