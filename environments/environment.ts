export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC7QVqbp9BG0v0ViCRTZx6lWqXlwrXH9Qk',
    authDomain: 'fussy-js.firebaseapp.com',
    databaseURL: 'https://fussy-js.firebaseio.com',
    projectId: 'fussy-js',
    storageBucket: 'fussy-js.appspot.com',
    messagingSenderId: '757930835086'
  },
};
